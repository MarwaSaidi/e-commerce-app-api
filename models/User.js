const mongoose = require("mongoose");
const UserSchema = new mongoose.Schema(
  { name:{ type: String, required: [true, "can't be required"], unique: true },
    lastname:{ type: String, required: [true, "can't be required"], unique: true },
    username: { type: String, required: true, unique: true },
    email: { type: String, lowercase: true, required: [true, "can't be required"], match: [/\S+@\S+\.\S+/, 'is invalid'], index: true },
    password: { type: String, required: true },
    isAdmin: {
      type: Boolean,
      default: false,
    },
    img: { type: String },
  },
  { timestamps: true }
);

module.exports = mongoose.model("User", UserSchema);
